import atexit
import getpass
import logging
import os
import random
import shutil
import socket
from http.client import BadStatusLine, IncompleteRead
from string import ascii_lowercase
from subprocess import DEVNULL, PIPE, Popen
from time import monotonic, sleep, time
from urllib.parse import urljoin

import selenium
from pyvirtualdisplay import Display
from selenium.common.exceptions import (
    NoSuchElementException,
    StaleElementReferenceException,
    TimeoutException,
    WebDriverException,
)
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.alert import Alert
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.remote.remote_connection import RemoteConnection
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from .constants import Selectors
from .locks import DirLock
from .utils import PopenPatcher


__all__ = ["Browser", "BrowserException", "BrowserUnavailableError"]

logger = logging.getLogger(__name__)

RemoteConnection.set_timeout(150)

GET_XPATH_JS_FUNC = """var result = [], argLen = arguments.length, node;for (var j=0; j < argLen; j++) {node = arguments[j];var stack = [];var neighbours, l, index, tagName;while(node.parentNode !== null) {neighbours = node.parentNode.children;tagName = node.tagName;l = neighbours.length;index = 1;for (var i=0; i < l; i++) {if (tagName === neighbours[i].tagName) {if (node !== neighbours[i]) {index = index + 1;} else {break;}}}if (tagName.toLowerCase() == 'ul') {tagName = '/'+tagName;}if (index == 1) {stack.unshift(tagName);} else {stack.unshift(tagName+'['+index+']');}node = node.parentNode;}result.push(stack.join('/'));}return result;"""
CLEAR_LOCAL_STORAGE = """localStorage.clear();"""


class TimeoutChecker:
    def __init__(self, timeout, exc=None):
        self.end_time = timeout + time()
        self.exc = exc or Exception

    def __call__(self):
        if time() > self.end_time:
            raise self.exc()


class BrowserException(Exception):
    pass


class BrowserUnavailableError(BrowserException):
    pass


class SilentCallable:
    def __init__(self, f, args=None, kwargs=None, inverse=False):
        self.f = f
        self.args = args or ()
        self.kwargs = kwargs or {}
        self.inverse = inverse

    def __call__(self, driver=None):
        try:
            if self.inverse:
                return not self.f(*self.args, **self.kwargs)
            return self.f(*self.args, **self.kwargs)
        except Exception:
            return None


class NewElements:
    def __init__(self, node):
        self.found_elements = set()
        self.node = node
        self.track_by = node.track_duplicates_by
        if self.track_by is None:
            from .elements import Node

            self.track_by = Node(target=Node.InnerTextHash)

    def get_id(self, elem):
        return (
            elem.id
            if self.track_by is None
            else self.track_by.get(instance=elem, timeout=0)
        )

    def get_new_elements(
        self, driver=None, _inner=False, _new_found_elements=None
    ):
        els = self.node.get_raw(simple=True, timeout=0)
        if not els:
            return
        if self.node.singular:
            els = [els]
        found_elements = self.found_elements

        new_found_elements = set()
        if _new_found_elements:
            new_found_elements.update(_new_found_elements)

        result = []
        for el in els:
            _id = self.get_id(el)
            if _id in found_elements or _id in new_found_elements:
                continue
            new_found_elements.add(_id)
            result.append(el)
        if not result:
            return

        if _inner:
            return result, new_found_elements

        else:
            for i in range(1):
                sleep(1)
                _tmp = self.get_new_elements(
                    driver, _inner=True, _new_found_elements=new_found_elements
                )
                if _tmp is None:
                    continue
                _result, _new_elems = _tmp
                if _result:
                    new_found_elements.update(_new_elems)
                    result.extend(_result)
            found_elements.update(new_found_elements)

        return result

    def __call__(self, driver=None, attempts=5):
        for i in range(1, attempts + 1):
            try:
                return self.get_new_elements(driver)
            except StaleElementReferenceException:
                if i == attempts:
                    raise


class BrowserProfilerCtxManager:
    def __init__(self, browser_instance, profile_timeouts):
        self.browser_instance = browser_instance
        self.profile_timeouts = profile_timeouts
        self.prev_profile_timeouts = None

    def __enter__(self):
        self.prev_profile_timeouts = self.browser_instance.profile_timeouts
        self.browser_instance.profile_timeouts = self.profile_timeouts

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.browser_instance.profile_timeouts = self.prev_profile_timeouts


class CustomChrome(selenium.webdriver.Chrome):
    DATA_DIR = "/tmp/chrome"

    def __init__(self, *args, **kwargs):
        self.unavailable_after = kwargs.pop("unavailable_after", None)
        if self.unavailable_after is None:
            self.unavailable_after = 120
        super().__init__(*args, **kwargs)
        self.is_available = True

    def execute(self, *args, **kwargs):
        t = time()
        try:
            return super().execute(*args, **kwargs)
        except (TimeoutException, socket.timeout) as e:
            timeout = time() - t
            if timeout > self.unavailable_after:
                self.is_available = False
                raise BrowserUnavailableError() from e
            raise

    @classmethod
    def new(cls, **kwargs):
        from selenium.webdriver import ChromeOptions

        downloads_dir = kwargs.pop("downloads_dir")

        chrome_opts = ChromeOptions()
        chrome_opts.binary_location = "/usr/bin/chromium"

        # Still doesn't work for iTunes sales reports loading
        # chrome_opts.add_argument("--headless")
        chrome_opts.add_argument("--no-sandbox")
        chrome_opts.add_argument("--window-size=1920,1080")
        chrome_opts.add_argument(f"--user-data-dir={cls.DATA_DIR}/user-data")
        chrome_opts.add_argument(f"--data-path={cls.DATA_DIR}/data-path")
        chrome_opts.add_argument(f"--homedir={cls.DATA_DIR}/")
        chrome_opts.add_argument(f"--disk-cache-dir={cls.DATA_DIR}/cache-dir")
        chrome_opts.add_argument("--disable-extensions")
        chrome_opts.add_argument("--safebrowsing-disable-extension-blacklist")
        chrome_opts.add_argument("--safebrowsing-disable-download-protection")
        chrome_opts.add_argument("--disable-gpu")
        chrome_opts.add_argument("--disable-sync")
        chrome_opts.add_argument("--disable-webgl")
        chrome_opts.add_argument("--hide-scrollbars")
        chrome_opts.add_argument("--dns-prefetch-disable")

        user_agent = kwargs.pop("user_agent", None)
        if user_agent:
            chrome_opts.add_argument(f"--user-agent={user_agent}")

        proxy_str = kwargs.pop("proxy_str", None)
        if proxy_str is not None:
            chrome_opts.add_argument(f"--proxy-server={proxy_str}")

        features = [
            "FreezeUserAgent",
        ]
        chrome_opts.add_argument(
            "--enable-features={}".format(",".join(features))
        )

        desired_caps = DesiredCapabilities.CHROME
        desired_caps.update(kwargs.pop("desired_capabilities", {}))

        prefs = {
            "download.default_directory": downloads_dir,
            "download.prompt_for_download": False,
            "download.directory_upgrade": True,
            "download.extensions_to_open": "*",
            "safebrowsing.enabled": False,
        }
        if kwargs.pop("enable_images", False):
            prefs["webkit.webprefs.loads_images_automatically"] = True
            prefs["profile.managed_default_content_settings.images"] = 1
        else:
            prefs["webkit.webprefs.loads_images_automatically"] = False
            prefs["profile.managed_default_content_settings.images"] = 2

        chrome_opts.add_experimental_option("prefs", prefs)

        for name, capability in desired_caps.items():
            chrome_opts.set_capability(name, capability)

        browser = cls(
            options=chrome_opts,
            unavailable_after=kwargs.pop("default_pageload_timeout"),
            **kwargs,
        )
        logger.debug("[BROWSER] Chrome driver started")

        return browser


class TinyProxyConfig:
    host = "127.0.0.1"
    port = 8888
    bin_path = "/usr/bin/tinyproxy"
    conf_path = "/etc/tinyproxy/tinyproxy.conf"


class Browser:
    """
    Hang issues:
    - https://code.google.com/p/selenium/issues/detail?id=6955
    """

    FIREFOX_SCREEN_SIZE = (1920, 1080)
    FIREFOX_IS_VISIBLE = False
    DEFAULT_PARSER_TIMEOUT = 15
    DEFAULT_INFINITE_SCROLL_TIMEOUT = DEFAULT_PARSER_TIMEOUT
    BROWSER_LIFETIME = 300

    Exception = BrowserException
    crash_exceptions = (
        ConnectionError,
        BadStatusLine,
        IncompleteRead,
        ConnectionRefusedError,
        socket.timeout,
        BrowserUnavailableError,
    )

    downloads_dir = os.path.join(os.getcwd(), "browser_downloads")
    _none = object()
    _blank_tuple = ()

    os.makedirs(downloads_dir, exist_ok=True)

    DownloadsDirLock = DirLock.configure(
        "browser_downloads",
        dir_name=downloads_dir,
        expire=DEFAULT_PARSER_TIMEOUT,
    )
    browser_loading_exts = (".crdownload", ".part")
    partial_filenames_to_skip = (".com.google.Chrome.",)

    TINY_PROXY_CONFIG = TinyProxyConfig

    subprocesses = [
        "chromium",
        "chromedriver",
        "Xvfb",
        "tinyproxy",
    ]

    _popen_patcher = PopenPatcher(*subprocesses)

    @staticmethod
    def instance(reset=False, reset_on_fail=False, **kwargs) -> "Browser":
        instance = getattr(Browser, "_instance", None)

        if (
            reset
            or not instance
            or not instance._is_alive(
                check_lifetime=reset_on_fail,
                suppress_connection_refused=reset_on_fail,
            )
        ):
            exc = None

            for i in range(10):
                instance = getattr(Browser, "_instance", None)

                if instance:
                    instance.close()
                else:
                    Browser.kill_all()

                try:
                    Browser._instance = Browser(**kwargs)
                except WebDriverException as e:
                    exc = e
                    logger.warning("Exception in starting browser: %s", str(e))
                except Exception as e:
                    if (reset or reset_on_fail) and Browser.is_crash_exception(
                        e
                    ):
                        exc = e
                        continue

                    raise
                else:
                    exc = None
                    break

            if exc:
                raise exc

        return Browser._instance

    def _is_alive(
        self,
        suppress_connection_refused=True,
        suppress_all=False,
        check_lifetime=False,
    ):
        if check_lifetime:
            not_alive_by_time = (
                time() - self.BROWSER_LIFETIME
            ) >= self.initialized_at
            if not_alive_by_time:
                return False
        try:
            self.current_url
        except Exception as e:
            if suppress_connection_refused and Browser.is_crash_exception(e):
                return False
            if suppress_all:
                return False
            raise e
        return True

    @staticmethod
    def is_alive(*args, simple=False, **kwargs):
        instance = getattr(Browser, "_instance", None)
        if not instance:
            return False
        if instance.driver and not instance.driver.is_available:
            return False
        if simple:
            return True

        return instance._is_alive(*args, **kwargs)

    @classmethod
    def is_crash_exception(cls, exc):
        exc_str = str(exc)
        return (
            isinstance(exc, cls.crash_exceptions)
            or "A script on this page may be busy" in exc_str
            or "chrome not reachable" in exc_str
            or "timed out" in exc_str
        )

    @staticmethod
    def halt():
        if hasattr(Browser, "_instance"):
            Browser._instance.close()
            delattr(Browser, "_instance")

    def __init__(self, timeout=None, **kwargs):
        self.DEFAULT_PARSER_TIMEOUT = timeout or self.DEFAULT_PARSER_TIMEOUT
        self.DEFAULT_PAGELOAD_TIMEOUT = self.DEFAULT_PARSER_TIMEOUT * 8

        display = Display(
            visible=kwargs.pop("visible", self.FIREFOX_IS_VISIBLE),
            size=self.FIREFOX_SCREEN_SIZE,
        )
        display.start()
        self.display = display

        logger.debug("[BROWSER] VirtualDisplay initialized")

        proxy_url = kwargs.pop("proxy_url", None)
        if proxy_url:
            # remove http[s]:// and run proxy
            t_proxy_url = self._run_tinyproxy(proxy_url.split("://", 1)[-1])
            kwargs["proxy_str"] = t_proxy_url

        driver = CustomChrome.new(
            downloads_dir=self.downloads_dir,
            default_pageload_timeout=self.DEFAULT_PAGELOAD_TIMEOUT,
            **kwargs,
        )
        driver.set_page_load_timeout(self.DEFAULT_PAGELOAD_TIMEOUT)
        driver.set_script_timeout(self.DEFAULT_PARSER_TIMEOUT)
        self.driver = driver

        self._dirty_timeout = False
        self.initialized_at = time()
        self.default_wait = WebDriverWait(
            self.driver, self.DEFAULT_PARSER_TIMEOUT
        )
        self.profile_timeouts = None

    @classmethod
    def _run_tinyproxy(cls, proxy_url):
        path = os.path.dirname(os.path.abspath(__file__))

        with open(path + "/tinyproxy.conf") as conf_template_file:
            conf = conf_template_file.read().format(
                user=getpass.getuser(),
                group=getpass.getuser(),
                port=TinyProxyConfig.port,
                proxy_url=proxy_url,
            )

        with open(TinyProxyConfig.conf_path, "w") as conf_file:
            conf_file.write(conf)

        tinyproxy_process = Popen([TinyProxyConfig.bin_path], stdout=PIPE)
        output = tinyproxy_process.communicate()[0].decode("UTF-8")

        if output:
            logger.debug(f"[BROWSER] [TINYPROXY] {output}")
        elif not tinyproxy_process.errors:
            logger.debug(f"[BROWSER] TinyProxy started")

        return f"{TinyProxyConfig.host}:{TinyProxyConfig.port}"

    @classmethod
    def kill_all(cls):
        cls.clear_data_dir()
        with Popen(
            [
                "killall",
                "-s",
                "9",
                "-w",
            ]
            + cls.subprocesses,
            stdout=DEVNULL,
            stderr=DEVNULL,
        ) as p:
            try:
                for i in range(100):
                    if i > 10 and p.poll() is not None:
                        break
                    try:
                        awaited_zombie_pid = os.waitpid(-1, os.WNOHANG)[0]
                        if awaited_zombie_pid:
                            logger.debug(
                                "awaited zombie pid %d", awaited_zombie_pid
                            )
                    except ChildProcessError:
                        break
                    sleep(0.05)
            finally:
                try:
                    p.kill()
                except ProcessLookupError:
                    pass
        cls.ensure_browser_process_kill()

    @classmethod
    def ensure_browser_process_kill(cls):
        # find all active browser  processes
        ps = Popen(
            f"ps aux | grep {cls.subprocesses[0]}", stdout=PIPE, shell=True
        )
        output, error = ps.communicate()
        # example output:
        # ['braven    8818  0.0  0.0      0     0 pts/1    Z+   Oct02   0:00 [chromedriver] <defunct>',
        # 'braven    9602  0.0  0.0   4280   756 ?        S    07:27   0:00 /bin/sh -c ps aux | grep chromium',
        # 'braven    9604  0.0  0.0  12784  1012 ?        S    07:27   0:00 grep chromium']

        # skip  defunct (dead) processes
        output = list(
            filter(
                lambda cmd: cmd and "<defunct>" in cmd,
                output.decode("utf-8").split("\n"),
            )
        )

        if len(output) > 2:
            # two commands necessarily: [/bin/sh -c ps aux | grep chromium', 'grep chromium']
            raise Exception("Failed to kill all browser instances")

    def close(self):
        if hasattr(self, "_closed"):
            return
        self._closed = True

        try:
            self.driver.quit()
        except Exception:
            pass

        try:
            self.display.stop()
        except Exception:
            pass

        self.kill_all()

    def profiler(self, timeouts_gte):
        return BrowserProfilerCtxManager(self, timeouts_gte)

    def drop_page(self):
        self.driver.get("about:blank")

    def get(
        self, url, timeout=None, attempts=3, drop_current=True, check_url=None
    ):
        if drop_current:
            self.drop_page()
        if self._dirty_timeout:
            self.driver.set_page_load_timeout(self.DEFAULT_PAGELOAD_TIMEOUT)
            self._dirty_timeout = False
        if timeout is not None:
            self.driver.set_page_load_timeout(timeout)
            self._dirty_timeout = True

        if attempts == 1:
            self.driver.get(url)
        else:
            exc = None
            for i in range(attempts):
                if i:
                    logger.debug("Retrying getting to %s", url)
                try:
                    self.driver.get(url)
                    exc = None
                    break
                except Exception as e:
                    exc = e
            if exc is not None:
                raise exc

        if check_url and check_url not in self.driver.current_url:
            raise BrowserException(
                "Failed to get {} url, got {}".format(
                    url, self.driver.current_url
                )
            )

    def __getattr__(self, item):
        driver = object.__getattribute__(self, "driver")
        return getattr(driver, item)

    def __del__(self):
        self.close()

    def __repr__(self):
        return "[In-Celery Browser]"

    def get_element_by_xpath(self, xpath, timeout=DEFAULT_PARSER_TIMEOUT):
        return self.wait_and_get(
            Selectors.XPATH_SINGLE, xpath, timeout=timeout, silent=True
        )

    def has_more_than(self, prev_elem_number, node, instance):
        def func(x):
            new_elems = node.get_raw(instance, simple=True)
            if len(new_elems) and len(new_elems) > prev_elem_number:
                return new_elems
            else:
                return None

        return func

    def scroll_down(self, height="undefined"):
        self.driver.execute_script(
            "window.scrollTo(0,{}||document.body.scrollHeight)".format(height)
        )

    def scroll_down_for_elems(self, node, **kwargs):
        try:
            window_height = self.driver.execute_script(
                "return window.innerHeight"
            )
            scroll_height = self.driver.execute_script(
                "return document.body.scrollHeight"
            )
            new_scroll_position = 0

            new_elements_callable = NewElements(node)
            new_elements = self.wait_until(new_elements_callable)
            while new_elements or new_scroll_position < scroll_height:
                if new_elements is not None:
                    for elem in new_elements:
                        yield elem

                new_scroll_position += window_height
                self.scroll_down(new_scroll_position)

                new_elements = self.wait_until(new_elements_callable)

                scroll_height = self.driver.execute_script(
                    "return document.body.scrollHeight"
                )
        except TimeoutException:
            pass

    def scroll_to_elem(self, elem):
        assert elem
        failed = False
        try:
            try:
                self.driver.execute_script(
                    'arguments[0].scrollIntoView({block: "center", inline: "nearest"});',
                    elem,
                )
            except WebDriverException:
                logger.debug("Failed to scroll into the view")
                failed = True
            try:
                ActionChains(self.driver).move_to_element(elem).perform()
            except WebDriverException:
                logger.debug("Failed to move to the element")
                failed = True
        except Exception as e:
            logger.exception(e)
        return not failed

    def paginate_for_elems(self, node, **kwargs):
        next_page_link_node = node.paginate_by_node
        new_elements_callable = NewElements(node)
        try:
            new_elements = self.wait_until(
                new_elements_callable,
                timeout=kwargs.get("pagination_timeout", None),
            )
            while new_elements:
                logger.debug(
                    "Pagination: found %s elements", len(new_elements)
                )
                for elem in new_elements:
                    yield elem
                next_page_link = next_page_link_node.get(timeout=0)
                if not next_page_link:
                    return
                self.click_on_element(next_page_link)
                new_elements = self.wait_until(
                    new_elements_callable,
                    timeout=kwargs.get("pagination_timeout", None),
                )

        except TimeoutException:
            pass

    def accept_alert(self, silent=True):
        try:
            Alert(self).accept()
        except WebDriverException:
            if not silent:
                raise

    def click_on_element(
        self,
        elem,
        wait_after_click=None,
        attempts=1,
        timeout=None,
        force_js=False,
        default=None,
    ):
        assert elem

        if callable(wait_after_click):
            for i in range(attempts):
                if self.click_on_element(elem, force_js=force_js):
                    result = self.wait_until(
                        wait_after_click, timeout=timeout, default=default
                    )
                    if result:
                        return result
            return

        if force_js:
            self.execute_script("arguments[0].click()", elem)
        else:
            self.scroll_to_elem(elem)
            try:
                elem.click()
            except WebDriverException as e:
                e_str = str(e)
                if (
                    "Element is not clickable" in e_str
                    or "Other element would receive the click" in e_str
                ):
                    self.click_on_element(elem, force_js=True)
                else:
                    raise
        sleep(wait_after_click or 0)
        return True

    def wait_and_get(
        self,
        selector,
        attr,
        instance=None,
        timeout=None,
        silent=False,
        only_visible=False,
        scroll_to=False,
        default=_none,
        ts=None,
    ):
        by = selector.by
        expected_condition = (
            selector.visibility_condition
            if only_visible
            else selector.presence_condition
        )(by, attr, instance)

        if ts is not None:
            timeout = max(
                (self.DEFAULT_PARSER_TIMEOUT if timeout is None else timeout)
                - time()
                + ts,
                0,
            )

        wait = (
            WebDriverWait(self.driver, timeout)
            if timeout
            else self.default_wait
        )
        try:
            if timeout == 0:
                result = expected_condition(self.driver)
            elif self.profile_timeouts is not None:
                _t = time()
                try:
                    result = wait.until(expected_condition)
                finally:
                    elapsed = time() - _t
                    if elapsed > self.profile_timeouts:
                        logger.debug(
                            "Waited %s for %s by %s", elapsed, attr, selector
                        )
            else:
                result = wait.until(expected_condition)

            if scroll_to and isinstance(result, WebElement):
                self.scroll_to_elem(result)
            return result
        except NoSuchElementException:
            if default is self._none:
                if selector.singular:
                    return None
                return self._blank_tuple
            return default

        except StaleElementReferenceException:
            raise

        except TimeoutException:
            if not silent:
                raise
            if timeout != 0:
                logger.debug("Timeout %s", attr)
            if default is self._none:
                if selector.singular:
                    return None
                return self._blank_tuple
            return default

    def wait_until_not(
        self,
        func,
        args=None,
        kwargs=None,
        timeout=None,
        silent=True,
        default=None,
    ):
        wait = (
            timeout is not None
            and WebDriverWait(self.driver, timeout)
            or self.default_wait
        )
        if args or kwargs:
            func = SilentCallable(func, args, kwargs)
        try:
            return wait.until_not(func)
        except StaleElementReferenceException:
            raise
        except TimeoutException:
            if timeout != 0:
                logger.debug("Timeout of waiting while %s", func)
            if not silent:
                raise
            return default

    def wait_until(
        self,
        func,
        args=None,
        kwargs=None,
        timeout=None,
        silent=True,
        default=None,
    ):
        wait = (
            timeout is not None
            and WebDriverWait(self.driver, timeout)
            or self.default_wait
        )
        if args or kwargs:
            func = SilentCallable(func, args, kwargs)
        try:
            return wait.until(func)
        except StaleElementReferenceException:
            raise
        except TimeoutException:
            if timeout != 0:
                logger.debug("Timeout of waiting until %s", func)
            if not silent:
                raise
            return default

    def poll(self, name_to_func_or_node: dict, timeout=15, poll_cooldown=0.5):
        from .elements import Node

        end_time = monotonic() + timeout
        while True:
            for name, func_or_node in name_to_func_or_node.items():
                value = (
                    func_or_node.get(timeout=0)
                    if isinstance(func_or_node, Node)
                    else func_or_node()
                )
                if value:
                    return name, value

            if monotonic() + poll_cooldown > end_time:
                break

            sleep(poll_cooldown)

        return None, None

    def wait_for_staleness_of(self, element, timeout=None, silent=True):
        wait = (
            timeout is not None
            and WebDriverWait(self.driver, timeout)
            or self.default_wait
        )
        try:
            return wait.until(EC.staleness_of(element))
        except StaleElementReferenceException:
            raise
        except TimeoutException:
            if timeout != 0:
                logger.debug("Timeout of waiting for staleness of element")
            if not silent:
                raise

    def get_xpaths(self, *elements):
        result = self.execute_script(GET_XPATH_JS_FUNC, *elements)
        return result[0] if len(result) == 1 else result

    def purge_user_data(self):
        self.execute_script(CLEAR_LOCAL_STORAGE)
        self.delete_all_cookies()

    def get_page_content(self):
        return self.driver.execute_script(
            "return document.documentElement.innerHTML"
        )

    def set_page_content(self, content, buffer_size=100000):
        self.drop_page()
        for i in range(0, len(content), buffer_size):
            self.driver.execute_script(
                (
                    "window.content += arguments[0];"
                    if i
                    else "window.content = arguments[0];"
                ),
                content[i : i + buffer_size],
            )
        self.driver.execute_script(
            "document.documentElement.innerHTML=window.content; window.content=undefined;"
        )

    @classmethod
    def resolve_href(cls, href, page_url):
        return urljoin(page_url, href)

    def save_screenshot(
        self, filename=None, cwd=None, save_html_snapshot=False
    ):
        cwd = cwd or os.getcwd()
        filename = filename or str(int(time()))
        file_path = "{}.png".format(os.path.join(cwd, filename))
        self.driver.save_screenshot(file_path)
        if save_html_snapshot:
            self.save_html_snapshot(filename, cwd)
        return file_path

    def save_html_snapshot(self, filename=None, cwd=None):
        cwd = cwd or os.getcwd()
        filename = filename or str(int(time()))
        file_path = "{}.html".format(os.path.join(cwd, filename))
        with open(file_path, "w") as f:
            page_content = self.get_page_content()
            if page_content:
                f.write(page_content)
                f.write("Current URL: {}".format(self.current_url))
        return file_path

    def get_download_dir_files(self):
        return set(
            fn
            for fn in os.listdir(self.downloads_dir)
            if not fn.endswith(".swp")
            and not any(
                p_filename in fn
                for p_filename in self.partial_filenames_to_skip
            )
        )

    def download_file(
        self,
        url=None,
        elem_to_click=None,
        no_file_check=None,
        timeout=DEFAULT_PARSER_TIMEOUT * 2,
        click_kwargs=None,
    ):
        lock = self.DownloadsDirLock(expire=timeout)
        no_file_check = no_file_check or (lambda: False)
        while not lock.acquire_lock():
            sleep(0.5)
        with lock.release_lock(ctx_manager=True):
            timeout_check = TimeoutChecker(timeout, exc=TimeoutException)
            initial_dir_files = self.get_download_dir_files()
            if url:
                self.get(url)
            elif elem_to_click:
                self.click_on_element(elem_to_click, **(click_kwargs or {}))
            else:
                raise ValueError(
                    "Invalid 'url' and(or) 'elem_to_click' parameters combination"
                )

            while True:
                sleep(0.5)
                if no_file_check():
                    return None
                new_filenames = list(
                    self.get_download_dir_files().difference(initial_dir_files)
                )
                assert len(new_filenames) < 3
                if len(new_filenames) == 1 and not any(
                    new_filenames[0].endswith(ext)
                    for ext in self.browser_loading_exts
                ):
                    sleep(1.5)
                    return os.path.join(self.downloads_dir, new_filenames[0])
                timeout_check()

    def safe_send_keys(
        self,
        elem_getter,
        value,
        clean=False,
        timeout=DEFAULT_PARSER_TIMEOUT,
        interval=0,
        silent=True,
    ):
        timeout_check = TimeoutChecker(timeout, exc=TimeoutException)
        try:
            while True:
                elem = elem_getter()
                if elem:
                    try:
                        if clean:
                            elem.clear()
                        elem.send_keys(value)
                        break
                    except WebDriverException:
                        pass
                timeout_check()
                sleep(interval)
        except TimeoutException:
            if silent:
                return False
            raise
        return True

    def screenshot_elem(self, elem, to_jpg=None, doc_offsets=(0, 0)):
        from io import BytesIO

        from PIL import Image

        self.scroll_to_elem(elem)
        rect = self.execute_script(
            "return arguments[0].getBoundingClientRect()", elem
        )
        png_content = self.get_screenshot_as_png()
        x1 = rect["x"] + doc_offsets[0]
        y1 = rect["y"] + doc_offsets[1]
        x2 = x1 + rect["width"]
        y2 = y1 + rect["height"]
        image = Image.open(BytesIO(png_content))
        image = image.crop((x1, y1, x2, y2))
        image_format = "PNG"
        if to_jpg:
            image = image.convert("RGB")
            image_format = "JPEG"

        tmp_io = BytesIO()
        image.save(tmp_io, format=image_format)
        tmp_io.seek(0)
        return tmp_io.read()

    def debug(self, html=False):
        if not hasattr(self, "_debug_filename"):
            self._debug_filename = "debug_chrome_{}".format(
                "".join([random.choice(ascii_lowercase) for i in range(3)])
            )
            logger.debug("Toggled debug: %s", self._debug_filename)
        self.save_screenshot(
            filename=self._debug_filename, save_html_snapshot=html
        )
        return self._debug_filename

    @classmethod
    def clear_data_dir(cls):
        # Recursively delete source directory
        try:
            shutil.rmtree(CustomChrome.DATA_DIR, True)
        except Exception:
            pass


atexit.register(Browser.halt)


try:
    from celery import signals as celery_signals
except ImportError:
    pass
else:

    @celery_signals.worker_process_shutdown.connect
    def clean_up(sender=None, conf=None, **kwargs):
        Browser.halt()
