import os
import string
from random import choice
from time import time


__all__ = ["DirLock"]


class ReleaseLockOnException:
    def __init__(self, target, ignore=tuple()):
        assert all(issubclass(i, Exception) for i in ignore)
        self.ignored_exceptions = ignore
        self.target = target

    def __enter__(self):
        pass

    def __exit__(self, exc_type, exc_val, exc_tb):
        if exc_type is not None and not any(
            exc_type is i for i in self.ignored_exceptions
        ):
            self.target.release_lock()
        return False


class ReleaseLock:
    def __init__(self, target):
        self.target = target

    def __enter__(self):
        pass

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.target.release_lock()
        return False


class DirLock:
    ReleaseLockOnException = ReleaseLockOnException
    ReleaseLock = ReleaseLock
    base_prefix = ".dir_lock_"
    dir_name = None
    expire = 60

    number = 0
    dir_locks_counter = 1

    @classmethod
    def configure(cls, *args, **kwargs):
        dir_name = kwargs.get("dir_name", cls.dir_name)
        expire = kwargs.get("expire", cls.expire)
        t = type(
            "{}{}".format(cls.__name__, str(cls.dir_locks_counter)),
            (cls,),
            {
                "dir_name": dir_name,
                "expire": expire,
                "number": cls.dir_locks_counter,
                "base_prefix": cls.base_prefix + "".join(map(str, args)),
            },
        )
        cls.dir_locks_counter += 1
        return t

    def __init__(self, *args, **kwargs):
        self.dir_name = kwargs.get("dir_name", self.dir_name) or os.getcwd()
        self.lock_base_name_prefix = "{}{}{}".format(
            self.base_prefix, str(self.number), "".join(map(str, args))
        )
        self.expire = kwargs.get("expire", self.expire)
        self.lock_basename = None
        self.lock_filename = None
        self.ts = None
        self.salt = None

    def _renew_lock_filename(self):
        self.ts = int(time()) + self.expire
        self.salt = "".join(
            choice(string.ascii_lowercase + string.digits) for i in range(8)
        )
        self.lock_basename = "{}__{}__{}".format(
            self.lock_base_name_prefix, self.ts, self.salt
        )
        self.lock_filename = os.path.join(self.dir_name, self.lock_basename)
        return self.lock_filename

    def _parse_lock_name(self, lock_name):
        assert self.lock_base_name_prefix in lock_name
        lock_name, ts, salt = os.path.basename(lock_name).split("__")
        return lock_name, int(ts), salt

    def is_lock(self, lock_name):
        return self.lock_base_name_prefix in lock_name

    def get_current_lock(self):
        filenames = sorted(os.listdir(self.dir_name))
        lock_name, ts, salt = None, None, None
        for filename in filenames:
            if self.is_lock(filename):
                _lock_name, _ts, _salt = self._parse_lock_name(filename)
                if lock_name is None and time() <= _ts:
                    lock_name, ts, salt = _lock_name, _ts, _salt
                else:
                    self._drop_lock(filename)
        return lock_name, ts, salt

    def check_lock(self):
        lock_name, ts, salt = self.get_current_lock()
        if lock_name is None:
            return None
        return (
            lock_name == self.lock_base_name_prefix
            and ts == self.ts
            and salt == self.salt
        )

    def renew_lock(self):
        lock_status = self.check_lock()
        if lock_status is False:
            return False

        prev_lock_filename = self.lock_filename
        self._renew_lock_filename()

        with open(self.lock_filename, "w"):
            pass

        if prev_lock_filename:
            self._drop_lock(prev_lock_filename)

        if self.check_lock():
            return True
        self._drop_lock(self.lock_filename)
        return False

    def acquire_lock(self):
        return self.renew_lock()

    @staticmethod
    def _drop_lock(lock_filename):
        try:
            os.remove(lock_filename)
        except FileNotFoundError:
            pass

    def release_lock(self, ctx_manager=False):
        if ctx_manager:
            return self.ReleaseLock(target=self)
        self._drop_lock(self.lock_filename)
