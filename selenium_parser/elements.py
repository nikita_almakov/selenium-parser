import hashlib
import logging
import time
from collections.abc import Iterable

from selenium.common.exceptions import (
    StaleElementReferenceException,
    WebDriverException,
)

from .browser import Browser
from .constants import Selectors


__all__ = ["Node", "TargetPollingNode"]


logger = logging.getLogger(__name__)


class AttrDict(dict):
    """AttrDict provides access to its values through attributes

    >>> attr_dict = AttrDict({"foo": "bar"})
    >>> attr_dict["foo"]
    'bar'
    >>> attr_dict.foo
    'bar'
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__dict__ = self


class NodeException(Exception):
    pass


class NodeTargetNotReady(NodeException):
    pass


class NodeHasClass:
    def __init__(self, class_name, invert=False, raise_exception=None):
        self.raise_exception = raise_exception
        self.class_name = class_name
        self.invert = invert

    def __call__(self, node, elem):
        if not elem:
            return None
        classes = elem.get_attribute("class") or ""
        result = self.class_name in classes.split(" ")
        if self.invert:
            result = not result
        if result:
            return True
        if self.raise_exception is not None:
            raise self.raise_exception()
        return False


class NodeIsOverlaid:
    def __call__(self, node, elem):
        if elem is None:
            return None
        browser = Browser.instance()
        browser.scroll_to_elem(elem)
        return browser.driver.execute_script(
            "e=arguments[0];r=e.getBoundingClientRect();el=document.elementFromPoint(r.left, r.top);while(e){if(el===e){return false;}e=e.parentElement;}return true;",
            elem,
        )


class TrackPathInException:
    def __init__(self, node):
        self.node = node
        self.selector = self.node.selector

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if exc_val and self.selector:
            _path = getattr(exc_val, "_path", ())
            is_first = not _path
            _path = exc_val._path = (self.selector,) + _path
            if is_first:
                exc_val.args += _path
            else:
                exc_val.args = exc_val.args[:-1] + _path


class RequiredDescriptor:
    """Helps to avoid ImportError due to circular imports"""

    def __init__(self):
        self.__required = None

    def __get__(self, instance, owner):
        if not self.__required:
            from .node_target_parsers import AssertValue

            self.__required = AssertValue(bool)
        return self.__required


class Node:
    TargetNotReady = NodeTargetNotReady
    InnerHTML = -1000
    InnerHTMLHash = -1001
    InnerText = -1002
    InnerTextHash = -1005
    InnerTextWithoutChildren = -1004
    Raw = -1003
    HasClass = NodeHasClass
    IsOverlaid = NodeIsOverlaid

    # shortcut for AssertValue(bool)
    Required = RequiredDescriptor()

    _none = object()

    def __init__(
        self,
        selector=None,
        selector_type=Selectors.CSS_MULTI,
        target=Raw,
        target_preprocess=None,
        target_postprocess=None,
        levels_upper=None,
        only_visible=False,
        infinite_scroll=False,
        paginate_by_node=None,
        track_duplicates_by=None,
        inherit_parent=True,
        timeout=None,
        scroll_to=False,
    ):
        self.default_browser_cls = Browser
        if paginate_by_node is not None:
            assert isinstance(
                paginate_by_node, Node
            ), "paginate_by_node is not instance of Node."
        self.inherit_parent = inherit_parent
        self.selector = selector
        self.selector_type = selector_type
        self.target = target
        self.target_preprocess = target_preprocess
        self.target_postprocess = target_postprocess
        self.levels_upper = levels_upper
        self.only_visible = only_visible
        self.singular = self.selector_type.singular or self.selector is None
        self.infinite_scroll = infinite_scroll
        if paginate_by_node is not None:
            assert (
                paginate_by_node.singular
            ), "Paginate By node should be singular"
        self.paginate_by_node = paginate_by_node
        self.track_duplicates_by = track_duplicates_by
        self.timeout = timeout
        self.scroll_to = scroll_to

    def get_raw(self, instance=None, simple=False, **kwargs):
        """Returns generator with elements."""
        browser = Browser.instance()
        instance = self.inherit_parent and instance or None
        if "timeout" not in kwargs and self.timeout is not None:
            kwargs["timeout"] = self.timeout
        if not simple and self.infinite_scroll:
            return browser.scroll_down_for_elems(
                node=self, instance=instance, **kwargs
            )
        elif not simple and self.paginate_by_node:
            return browser.paginate_for_elems(
                node=self, instance=instance, **kwargs
            )
        else:
            return browser.wait_and_get(
                self.selector_type,
                self.selector,
                instance,
                silent=kwargs.pop("silent", True),
                only_visible=self.only_visible,
                scroll_to=self.scroll_to,
                **kwargs
            )

    def get(self, instance=None, unprocessed=False, **kwargs):
        if "ts" not in kwargs:
            kwargs["ts"] = time.time()
        exc = None
        attempts = 3
        if self.infinite_scroll or self.paginate_by_node:
            attempts = 1

        with TrackPathInException(self):
            for i in range(attempts):
                try:
                    if self.selector is None:
                        element_or_elements = instance
                    else:
                        element_or_elements = self.get_raw(instance, **kwargs)

                    if not unprocessed and self.target_preprocess:
                        element_or_elements = self.target_preprocess(
                            element_or_elements
                        )

                    if self.target == self.Raw:
                        results = element_or_elements

                    elif self.singular:
                        results = self._get_element_attribute(
                            element_or_elements, self.target, kwargs
                        )
                    else:
                        results = (
                            [
                                self._get_element_attribute(
                                    elem, self.target, kwargs
                                )
                                for elem in element_or_elements
                            ]
                            if element_or_elements is not None
                            else []
                        )
                except StaleElementReferenceException as e:
                    if instance is not None and self.inherit_parent:
                        raise
                    exc = e
                    continue

                if not unprocessed and self.target_postprocess:
                    results = self.target_postprocess(results)

                return results

        raise exc

    def _get_element_attribute(self, element, target, node_kwargs):
        result = self._none
        if element:
            if isinstance(target, str):
                try:
                    if target == "xpath":
                        result = Browser.instance().get_xpaths(element)
                    else:
                        attr_or_property = element.get_attribute(target)
                        result = (
                            attr_or_property
                            or Browser.instance().execute_script(
                                "return arguments[0].getAttribute(arguments[1])",
                                element,
                                target,
                            )
                        ) or attr_or_property
                except WebDriverException:
                    result = None

            elif target == self.InnerHTML:
                try:
                    result = element.get_attribute("innerHTML")
                except WebDriverException:
                    result = None

            elif target == self.InnerHTMLHash:
                try:
                    result = hashlib.md5(
                        element.get_attribute("innerHTML").encode("utf-8")
                    ).digest()
                except WebDriverException:
                    result = None

            elif target == self.InnerText:
                text = element.get_attribute("textContent")
                result = text.strip() if isinstance(text, str) else text

            elif target == self.InnerTextHash:
                text = element.get_attribute("textContent")
                result = text.strip() if isinstance(text, str) else text
                result = hashlib.md5(result.encode("utf-8")).digest()

            elif target == self.InnerTextWithoutChildren:
                text = Browser.instance().execute_script(
                    'for(var n=arguments[0],t=n.firstChild,e="";t;)t.nodeType===Node.TEXT_NODE&&(e+=t.textContent),t=t.nextSibling;return e',
                    element,
                )
                result = text.strip() if isinstance(text, str) else text

            elif target == self.Raw:
                result = element

        if result is self._none:
            if isinstance(target, dict):
                result = AttrDict(
                    {
                        name: self._get_element_attribute(
                            element, attr, node_kwargs
                        )
                        for name, attr in target.items()
                    }
                )

            elif isinstance(target, Iterable) and not isinstance(target, str):
                result = [
                    self._get_element_attribute(element, attr, node_kwargs)
                    for attr in target
                ]

            elif isinstance(target, Node) and (
                element is not None
                or target.selector is None
                or self.selector is None
            ):
                instance = element
                if target.levels_upper is not None and element:
                    instance = element.find_element_by_xpath(
                        "./" + "/".join(("..",) * target.levels_upper)
                    )
                result = target.get(instance=instance, **node_kwargs)

            elif callable(target):
                try:
                    result = target(self, element)
                except WebDriverException:
                    result = None

        if result is self._none:
            if element is not None:
                raise Exception(
                    "Unsupported target {} of {}".format(target, element)
                )
            result = None
        return result


class TargetPollingNode(Node):
    """Intended to catch in-target Node.TargetNotReady exceptions and wait
    for ready target"""

    target_postprocess = None
    target_polling_interval = 1
    target_polling_timeout = Browser.DEFAULT_PARSER_TIMEOUT
    propagate_not_ready = False

    def __init__(self, *args, **kwargs):
        self.target_polling_interval = kwargs.pop(
            "target_polling_interval", self.target_polling_interval
        )
        self.target_polling_timeout = kwargs.pop(
            "target_polling_timeout", self.target_polling_timeout
        )
        self.propagate_not_ready = kwargs.pop(
            "propagate_not_ready", self.propagate_not_ready
        )
        super(TargetPollingNode, self).__init__(*args, **kwargs)

    @staticmethod
    def dummy_target_postprocess(data):
        return data

    def get(self, instance=None, **kwargs):
        timeout = kwargs.get(
            "target_polling_timeout",
            kwargs.get("timeout", self.target_polling_timeout),
        )
        first = True
        ts = time.time() + timeout
        while first or time.time() < ts:
            first = False
            try:
                return super(TargetPollingNode, self).get(instance, **kwargs)
            except Node.TargetNotReady:
                if self.propagate_not_ready:
                    raise
                time.sleep(self.target_polling_interval)
