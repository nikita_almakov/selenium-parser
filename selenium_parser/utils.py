# Don't kill the browser on CTRL+C
import os
import subprocess
from weakref import finalize


old_popen = None


class PopenPatcher:
    def __init__(self, *executables_to_start_in_new_process_group):
        self.executables_to_start_in_new_process_group = set(
            executables_to_start_in_new_process_group
        )
        self.old_popen = subprocess.Popen
        subprocess.Popen = self.patched_popen
        print(
            "PopenPatcher: patched Popen for "
            f"{executables_to_start_in_new_process_group}, so they are no "
            "longer killed by KeyboardInterrupt"
        )
        finalize(self, restore_original_popen, self.old_popen)

    def patched_popen(self, *args, **kwargs):
        executable = args[0][0]
        if executable in self.executables_to_start_in_new_process_group:
            if "preexec_fn" in kwargs:
                prev_f = kwargs["preexec_fn"]

                def f():
                    prev_f()
                    os.setpgrp()

                kwargs["preexec_fn"] = f
            else:
                kwargs["preexec_fn"] = os.setpgrp
        return self.old_popen(*args, **kwargs)


def restore_original_popen(original_popen):
    subprocess.Popen = original_popen
    print(f"PopenPatcher: restored original Popen")
