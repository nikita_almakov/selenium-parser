from enum import Enum

from selenium.common.exceptions import StaleElementReferenceException
from selenium.webdriver.common.by import By


__all__ = ["Selectors", "Actions"]


class BaseCondition:
    def __init__(self, by, attr, parent=None):
        self.by = by
        self.attr = attr
        self.parent = parent

    def __call__(self, driver):
        raise NotImplementedError()


class VisibilityOfElementsCondition(BaseCondition):
    """An expectation for checking that an element is present on the DOM of a
    page and visible. Visibility means that the element is not only displayed
    but also has a height and width that is greater than 0.
    locator - used to find the element
    returns the WebElement once it is located and visible
    """

    def __call__(self, driver):
        try:
            return [
                el
                for el in (self.parent or driver).find_elements(
                    self.by, self.attr
                )
                if el.is_displayed()
            ]
        except StaleElementReferenceException:
            return None


class VisibilityOfElementCondition(BaseCondition):
    """An expectation for checking that an element is present on the DOM of a
    page and visible. Visibility means that the element is not only displayed
    but also has a height and width that is greater than 0.
    locator - used to find the element
    returns the WebElement once it is located and visible
    """

    def __call__(self, driver):
        try:
            for el in (self.parent or driver).find_elements(
                self.by, self.attr
            ):
                if el.is_displayed():
                    return el
            return None
        except StaleElementReferenceException:
            return None


class PresenceOfElementsCondition(BaseCondition):
    def __call__(self, driver):
        try:
            return (self.parent or driver).find_elements(self.by, self.attr)
        except StaleElementReferenceException:
            return None


class PresenceOfElementCondition(BaseCondition):
    def __call__(self, driver):
        try:
            return (self.parent or driver).find_element(self.by, self.attr)
        except StaleElementReferenceException:
            return None


singularity_to_presence_condition_map = {
    True: PresenceOfElementCondition,
    False: PresenceOfElementsCondition,
}
singularity_to_visibility_condition_map = {
    True: VisibilityOfElementCondition,
    False: VisibilityOfElementsCondition,
}


class Selectors(Enum):
    # ALIAS = By, is_singular
    CSS_SINGLE = By.CSS_SELECTOR, True
    CSS_MULTI = By.CSS_SELECTOR, False
    ID_SINGLE = By.ID, True
    ID_MULTI = By.ID, False
    CLASS_SINGLE = By.CLASS_NAME, True
    CLASS_MULTI = By.CLASS_NAME, False
    PARTIAL_LINK_TEXT_SINGLE = By.PARTIAL_LINK_TEXT, True
    PARTIAL_LINK_TEXT_MULTI = By.PARTIAL_LINK_TEXT, False
    TAG_NAME_SINGLE = By.TAG_NAME, True
    TAG_NAME_MULTI = By.TAG_NAME, False
    NAME_SINGLE = By.NAME, True
    NAME_MULTI = By.NAME, False
    LINK_TEXT_SINGLE = By.LINK_TEXT, True
    LINK_TEXT_MULTI = By.LINK_TEXT, False
    XPATH_SINGLE = By.XPATH, True
    XPATH_MULTI = By.XPATH, False

    def __init__(self, *args, **kwargs):
        Enum.__init__(self)
        self.by, self.singular = self.value
        self.presence_condition = singularity_to_presence_condition_map[
            self.singular
        ]
        self.visibility_condition = singularity_to_visibility_condition_map[
            self.singular
        ]

    @classmethod
    def get_choices(cls):
        return tuple((item.name, item.name) for item in cls)


no_args = tuple()


class Actions(Enum):
    CLICK = ("click", no_args, ("on_element",))
    CLICK_AND_HOLD = ("click_and_hold", no_args, ("on_element",))
    CONTEXT_CLICK = ("context_click", no_args, ("on_element",))
    DOUBLE_CLICK = ("double_click", no_args, ("on_element",))
    DRAG_AND_DROP = ("drag_and_drop", ("source", "target"), no_args)
    DRAG_AND_DROP_BY_OFFSET = (
        "drag_and_drop_by_offset",
        ("source", "xoffset", "yoffset"),
        no_args,
    )
    KEY_DOWN = ("key_down", ("value",), ("element",))
    KEY_UP = ("key_up", ("value",), ("element",))
    MOVE_BY_OFFSET = ("move_by_offset", ("xoffset", "yoffset"), no_args)
    MOVE_TO_ELEMENT = ("move_to_element", ("to_element",), no_args)
    MOVE_TO_ELEMENT_WITH_OFFSET = (
        "move_to_element_with_offset",
        (
            "to_element",
            "xoffset",
            "yoffset",
        ),
        no_args,
    )
    RELEASE = ("release", no_args, ("on_element",))
    SEND_KEYS = ("send_keys", ("keys_to_send",), no_args)
    SEND_KEYS_TO_ELEMENT = (
        "send_keys_to_element",
        ("element", "keys_to_send"),
        no_args,
    )

    def __init__(self, *args):
        reverse_map = self.__class__.reverse_map = getattr(
            self.__class__, "reverse_map", {}
        )
        self._value_, self.required_args, self.optional_args = args
        reverse_map[self._name_] = reverse_map[self._value_] = self

    @classmethod
    def get_by_name(cls, value):
        return getattr(cls, "reverse_map", {})[value]

    @classmethod
    def get_choices(cls):
        return tuple((item.name, item.name) for item in cls)

    @staticmethod
    def get_expected_field_types_map():
        from .elements import Node

        return {
            "on_element": Node,
            "source": Node,
            "target": Node,
            "to_element": Node,
            "element": Node,
            "xoffset": int,
            "yoffset": int,
            "keys_to_send": str,
        }

    def validate_params(self, **kwargs):
        cls = self.__class__
        if not hasattr(cls, "_EXPECTED_FIELD_TYPES_MAP"):
            cls._EXPECTED_TYPE_FIELD_MAP = self.get_expected_field_types_map()
        expected_types = cls._EXPECTED_TYPE_FIELD_MAP
        for name, value in kwargs.items():
            if not name in expected_types:
                continue
            expected_type = expected_types[name]
            if value is not None and not isinstance(value, expected_type):
                raise Exception(
                    "{name}='{value}' is not an instance of "
                    "{expected_type}".format(
                        name=name,
                        value=value,
                        expected_type=expected_type.__name__,
                    )
                )
        absent_args = [
            arg
            for arg in self.required_args
            if arg not in kwargs
            or not isinstance(kwargs[arg], expected_types[arg])
        ]
        if absent_args:
            raise Exception(
                "{absent_args} {word} absent in {name} action.".format(
                    absent_args=", ".join(absent_args),
                    word="is" if len(absent_args) == 1 else "are",
                    name=self._name_,
                )
            )
