import logging
import re
import time
from datetime import date
from decimal import Decimal, InvalidOperation

from .elements import Node


__all__ = [
    "AssertLength",
    "AssertValue",
    "AssertValue",
    "AssertValueOrNotReady",
    "ChainParsers",
    "ParseDate",
    "ParseDecimal",
    "ParseInt",
    "ParsePercentage",
]

logger = logging.getLogger(__name__)


class BaseParse:
    def __init__(self, silent=False):
        self.silent = silent

    def __call__(self, target):
        if target is None and not self.silent:
            raise Node.TargetNotReady("{} - {}".format(self.__class__, target))
        return target


class ChainParsers:
    def __init__(self, *parsers):
        self.parsers = parsers

    def __call__(self, result):
        for parser in self.parsers:
            result = parser(result)
        return result


class ParseDate(BaseParse):
    def __init__(self, date_formats, *args, **kwargs):
        super(ParseDate, self).__init__(*args, **kwargs)
        self.date_formats = (
            (date_formats,) if isinstance(date_formats, str) else date_formats
        )

    def __call__(self, target):
        super(ParseDate, self).__call__(target)
        if target is None:
            return
        for date_format in self.date_formats:
            try:
                t = time.strptime(target, date_format)
                return date(t.tm_year, t.tm_mon, t.tm_mday)
            except ValueError:
                pass
        if not self.silent:
            raise Exception("Invalid target to parse date", target)


class NumberPrefixParserMixin:
    @staticmethod
    def get_factor(target):
        if not target:
            return 1
        target = target.lower()
        if "k" in target:
            return Decimal("1000")
        if "m" in target:
            return Decimal("1000000")
        if "b" in target:
            return Decimal("1000000000")
        return 1


class ParseDecimal(BaseParse, NumberPrefixParserMixin):
    pattern = re.compile(r"(-?[\d]+\.?[\d]*)")

    def __init__(self, *args, **kwargs):
        self.prefix_enabled = kwargs.pop("prefix_enabled", False)
        super(ParseDecimal, self).__init__(*args, **kwargs)

    def __call__(self, target):
        super(ParseDecimal, self).__call__(target)
        if target is None:
            return
        results = self.pattern.findall(target.strip(" ,"))
        results_len = len(results)
        if not results_len:
            raise Node.TargetNotReady()
        if results_len > 1:
            raise Exception("Invalid target to parse decimal", target)
        try:
            value = Decimal(results[0])
            if self.prefix_enabled:
                value *= self.get_factor(target)
            return value
        except InvalidOperation:
            raise Exception("Invalid target to parse decimal", target)


class ParsePercentage(BaseParse):
    pattern = re.compile(r"([\d]+\.?[\d]*)%")

    def __call__(self, target):
        super(ParsePercentage, self).__call__(target)
        if target is None:
            return
        results = self.pattern.findall(target.strip())
        results_len = len(results)
        if not results_len:
            raise Node.TargetNotReady()
        if results_len > 1:
            raise Exception("Invalid target to parse percentage", target)
        try:
            return Decimal(results[0]) / 100
        except InvalidOperation:
            raise Exception("Invalid target to parse percentage", target)


class ParseInt(BaseParse, NumberPrefixParserMixin):
    pattern = re.compile(r"([\d]+)")

    def __init__(self, *args, **kwargs):
        self.prefix_enabled = kwargs.pop("prefix_enabled", False)
        super(ParseInt, self).__init__(*args, **kwargs)

    def __call__(self, target):
        super(ParseInt, self).__call__(target)
        if target is None:
            return
        results = self.pattern.findall(target.strip())
        results_len = len(results)
        if not results_len:
            raise Node.TargetNotReady()
        if results_len > 1:
            raise Exception("Invalid target to parse int", target)
        try:
            value = int(results[0])
            if self.prefix_enabled:
                value *= self.get_factor(target)
            return value
        except ValueError:
            raise Exception("Invalid target to parse int", target)


class AssertValue(BaseParse):
    def __init__(self, value, error_msg=None):
        super(AssertValue, self).__init__()
        self.value = value
        self.error_msg = error_msg

    def __call__(self, target):
        assert (
            self.value(target)
            if callable(self.value)
            else target == self.value
        ), "Node assertion value error{}. Expected {}, got {}".format(
            (
                ": {}".format(self.error_msg)
                if self.error_msg is not None
                else ""
            ),
            self.value,
            target,
        )
        return target


class AssertValueOrNotReady(AssertValue):
    def __call__(self, *args, **kwargs):
        try:
            return super(AssertValueOrNotReady, self).__call__(*args, **kwargs)
        except AssertionError as e:
            raise Node.TargetNotReady(*e.args) from e


class AssertLength(BaseParse):
    def __init__(self, expected_length, return_index=None):
        self.expected_length = expected_length
        self.return_index = return_index

    def __call__(self, target):
        assert len(target) == self.expected_length
        if self.return_index is None:
            return target
        return target[self.return_index]
