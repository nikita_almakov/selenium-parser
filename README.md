# Installation

```bash
# install with `wire` feature
pip install git+https://bitbucket.org/nikita_almakov/selenium-parser#egg=selenium_parser[wire]
```

# Usage

### Proxies

TODO

### How to develop and debug

Add a `docker-compose.override.yml` file in `aprenita-infrastructure` that override the backend service: 
```yml
services:
  backend:
    volumes:
      - $PATH_TO_PROJECT/selenium-parser/selenium_parser:/opt/venv/lib/python3.7/site-packages/selenium_parser
```
Now the local version of `selenium-parser` is used as a package. 
